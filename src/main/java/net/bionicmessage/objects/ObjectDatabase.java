/*
 * ObjectDatabase.java
 * Copyright 2010 Mathew McBride
 *
 * Redistribution and use in source and binary forms, with or without modification, are
 * permitted provided that the following conditions are met:
 * 
 *    1. Redistributions of source code must retain the above copyright notice, this list of
 *       conditions and the following disclaimer.
 * 
 *    2. Redistributions in binary form must reproduce the above copyright notice, this list
 *       of conditions and the following disclaimer in the documentation and/or other materials
 *       provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY Mathew McBride ``AS IS'' AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
 * FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 * ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 * The views and conclusions contained in the software and documentation are those of the
 * authors and should not be interpreted as representing official policies, either expressed
 * or implied, of Mathew McBride.
 */
package net.bionicmessage.objects;

import java.io.File;
import java.sql.Clob;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ObjectDatabase {

    File dbpath = null;
    String dburl = null;
    private Connection conn = null;
    int itemCount = 100;
    private static final String CREATE_OBJECTS_SQL = "CREATE TABLE groupdav_objects (obj_source varchar(48),obj_url varchar(255),obj_etag varchar(255),obj_uid varchar(255),obj_name varchar(255),obj_dtstart timestamp,obj_dtend timestamp,obj_content CLOB,UNIQUE(obj_url),UNIQUE(obj_uid));";
    private static final String CREATE_OBJECTS_UID_INDEX = "CREATE INDEX url_index ON groupdav_objects(obj_url);";
    private static final String INSERT_INTO_OBJECTS_QUERY = "INSERT INTO groupdav_objects (obj_source, obj_url, obj_etag, obj_uid, obj_name, obj_dtstart, obj_dtend, obj_content) VALUES (?,?,?,?,?,?,?,?);";
    private static final String GET_OBJECT_UID_QUERY = "SELECT obj_content FROM groupdav_objects WHERE obj_uid = ?";
    private static final String GET_URL_ETAG_FROM_STORE_QUERY = "SELECT obj_url, obj_etag FROM groupdav_objects WHERE obj_source = ?";
    private static final String GET_URL_FOR_UID_QUERY = "SELECT obj_url FROM groupdav_objects WHERE obj_uid = ?";
    private static final String GET_ITEM_COUNT = "SELECT COUNT(*) AS \"count\" FROM GROUPDAV_OBJECTS";
    private static final String UPDATE_ETAG_NAME_DATE_QUERY = "UPDATE groupdav_objects SET obj_etag = ?, obj_name = ?,obj_dtstart = ?, obj_dtend = ?, obj_content = ? WHERE obj_uid = ?";
    private static final String GET_ETAG_FROM_UID_QUERY = "SELECT obj_etag FROM groupdav_objects WHERE obj_uid = ?";
    private static final String GET_URL_ETAG_FROM_UID_QUERY = "SELECT obj_url, obj_etag FROM groupdav_objects WHERE obj_uid = ?";
    private static final String GET_UID_FROM_NAME_START_END = "SELECT obj_uid FROM GROUPDAV_OBJECTS WHERE obj_name LIKE ? AND obj_dtstart = ? AND obj_dtend = ? ";
    private static final String GET_UID_FROM_NAME = "SELECT obj_uid FROM groupdav_objects WHERE obj_name LIKE ?";
    private static final String GET_UIDS_WHERE_URL_IN_ARRAY_BASE = "SELECT obj_uid, obj_url FROM groupdav_objects WHERE obj_url IN (";

    private static final String CREATE_VIRTUAL_OBJECTS = "CREATE TABLE virtual_objects ( " +
            "virtual_guid VARCHAR(255)," +
            "virtual_id VARCHAR(255)," +
            "parent_id VARCHAR(255))";

    public ObjectDatabase(String path) {
        this.dbpath = new File(path);
        this.dbpath.mkdirs();
        String dpath = this.dbpath.getAbsolutePath() + File.separatorChar;
        this.dburl = "jdbc:h2:" + dpath + "obtrack";
    }

    public void open()
            throws SQLException, ClassNotFoundException {
        Class.forName("org.h2.Driver");
        this.conn = DriverManager.getConnection(this.dburl);

        Statement doTablesExist = this.conn.createStatement();
        doTablesExist.execute("SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLES.TABLE_NAME = 'GROUPDAV_OBJECTS'");
        ResultSet rs = doTablesExist.getResultSet();
        if (!(rs.next())) {
            createTables();
        } else {
            setItemCount();
        }
        rs.close();
    }

    public void close()
            throws SQLException {
        this.conn.close();
    }

    public void purge() {
        File[] dbFiles = this.dbpath.listFiles();
        for (int i = 0; i < dbFiles.length; ++i) {
            File file = dbFiles[i];
            file.delete();
        }
    }

    protected void createTables()
            throws SQLException {
        Statement createObjectsTable = this.conn.createStatement();
        createObjectsTable.execute(CREATE_OBJECTS_SQL);
        createObjectsTable.close();

        Statement createObjectsUidIndex = this.conn.createStatement();
        createObjectsUidIndex.execute(CREATE_OBJECTS_UID_INDEX);
        createObjectsUidIndex.close();

        Statement createVirtualTable = this.conn.createStatement();
        createVirtualTable.execute(CREATE_VIRTUAL_OBJECTS);
        createVirtualTable.close();
    }

    public void createObject(String sourceName, String uid, String url, String etag, String name, String contents, Timestamp dtstart, Timestamp dtend)
            throws Exception {
        PreparedStatement ps = this.conn.prepareStatement(INSERT_INTO_OBJECTS_QUERY);
        ps.setString(1, sourceName);
        ps.setString(2, url);
        ps.setString(3, etag);
        ps.setString(4, uid);
        ps.setString(5, name);
        if (dtstart != null) {
            ps.setTimestamp(6, dtstart);
        } else {
            ps.setNull(6, java.sql.Types.TIMESTAMP);
        }
        if (dtend != null) {
            ps.setTimestamp(7, dtend);
        } else {
            ps.setNull(7,java.sql.Types.TIMESTAMP);
        }
        ps.setString(8, contents);
        boolean executed = ps.execute();
        ps.close();
    }

    public String getObjectByUid(String uid)
            throws SQLException {
        PreparedStatement getStatement = this.conn.prepareStatement(GET_OBJECT_UID_QUERY);

        getStatement.setString(1, uid);
        getStatement.execute();
        ResultSet rs = getStatement.getResultSet();
        if (!(rs.next())) {
            rs.close();
            return null;
        }
        Clob contentClob = rs.getClob("obj_content");
        String content = contentClob.getSubString(1, (int) contentClob.length());
        contentClob.free();
        rs.close();
        return content;
    }

    public void deleteObjectByUid(String uid) throws SQLException {
        PreparedStatement deleteStatement = this.conn.prepareStatement("DELETE FROM groupdav_objects WHERE obj_uid = ?");
        deleteStatement.setString(1, uid);
        boolean deleted = deleteStatement.execute();
        deleteStatement.close();
    }

    public Map<String, String> getUrlEtagMapForStore(String store) throws SQLException {
        PreparedStatement stmt = this.conn.prepareStatement(GET_URL_ETAG_FROM_STORE_QUERY);

        stmt.setString(1, store);
        stmt.execute();
        ResultSet rs = stmt.getResultSet();
        HashMap resultMap = new HashMap(this.itemCount);
        while (rs.next()) {
            String url = rs.getString("obj_url");
            String etag = rs.getString("obj_etag");
            resultMap.put(url, etag);
        }
        stmt.close();
        return resultMap;
    }

    public String getObjectURL(String uid)
            throws SQLException {
        PreparedStatement stmt = this.conn.prepareStatement(GET_URL_FOR_UID_QUERY);
        stmt.setString(1, uid);
        stmt.execute();
        ResultSet rs = stmt.getResultSet();
        if (!(rs.next())) {
            stmt.close();
            return null;
        }
        String url = rs.getString("obj_url");
        stmt.close();
        return url;
    }

    public void updateObject(String uid, String newEtag, String newName, Timestamp dtstart, Timestamp dtend, String contents)
            throws SQLException {
        PreparedStatement stmt = this.conn.prepareStatement(UPDATE_ETAG_NAME_DATE_QUERY);
        stmt.setString(1, newEtag);
        stmt.setString(2, newName);
        if (dtstart == null) {
            stmt.setNull(3, java.sql.Types.TIMESTAMP);
        } else {
            stmt.setTimestamp(3, dtstart);
        }
        if (dtend == null) {
            stmt.setNull(4, java.sql.Types.TIMESTAMP);
        } else {
            stmt.setTimestamp(4, dtend);
        }
        stmt.setString(5, contents);
        stmt.setString(6, uid);
        stmt.execute();
        stmt.close();
    }

    public String getEtagForUid(String uid) throws SQLException {
        PreparedStatement stmt = this.conn.prepareStatement(GET_ETAG_FROM_UID_QUERY);
        stmt.setString(1, uid);
        stmt.execute();
        ResultSet rs = stmt.executeQuery();
        if (!(rs.next())) {
            stmt.close();
            return null;
        }
        String etag = rs.getString("obj_uid");
        stmt.close();
        return etag;
    }

    public String[] getUrlEtagForUid(String uid)
            throws SQLException {
        PreparedStatement stmt = this.conn.prepareStatement(GET_URL_ETAG_FROM_UID_QUERY);
        stmt.setString(1, uid);
        stmt.execute();
        ResultSet rs = stmt.executeQuery();
        if (!(rs.next())) {
            stmt.close();
            return null;
        }
        String[] ret = new String[2];
        ret[0] = rs.getString("obj_url");
        ret[1] = rs.getString("obj_etag");
        stmt.close();
        return ret;
    }

    public ArrayList<Object[]> getAllData() throws SQLException {
        Statement stmt = this.conn.createStatement();
        stmt.execute("SELECT * FROM groupdav_objects;");
        ResultSet rs = stmt.getResultSet();
        ArrayList list = new ArrayList(this.itemCount);
        while (rs.next()) {
            Object[] data = new Object[8];
            data[0] = rs.getString("obj_uid");
            data[1] = rs.getString("obj_url");
            data[2] = rs.getString("obj_etag");
            data[3] = rs.getString("obj_source");
            data[4] = rs.getString("obj_name");
            data[5] = rs.getTimestamp("obj_dtstart");
            data[6] = rs.getTimestamp("obj_dtend");
            Clob dataClob = rs.getClob("obj_content");
            String content = dataClob.getSubString(1, (int) dataClob.length());
            dataClob.free();
            data[7] = content;
            list.add(data);
        }
        stmt.close();
        return list;
    }

    public String findUidForNameAndDate(String name, Timestamp dtstart, Timestamp dtend)
            throws SQLException {
        PreparedStatement stmt = null;
        if ((dtstart != null) && (dtend != null)) {
            stmt = this.conn.prepareStatement(GET_UID_FROM_NAME_START_END);
        } else {
            stmt = this.conn.prepareStatement(GET_UID_FROM_NAME);
        }
        stmt.setString(1, name.trim());
        if ((dtstart != null) && (dtend != null)) {
            stmt.setTimestamp(2, dtstart);
            stmt.setTimestamp(3, dtend);
        }
        stmt.execute();
        ResultSet rs = stmt.getResultSet();
        if (!(rs.next())) {
            stmt.close();
            return null;
        }
        String matchingUid = rs.getString("obj_uid");
        stmt.close();
        return matchingUid;
    }

    public ArrayList<String> getAllUIDS() throws SQLException {
        Statement stmt = this.conn.createStatement();
        stmt.execute("SELECT obj_uid FROM groupdav_objects");
        ResultSet rs = stmt.getResultSet();
        ArrayList uids = new ArrayList(this.itemCount);
        while (rs.next()) {
            uids.add(rs.getString("obj_uid"));
        }
        stmt.close();
        return uids;
    }

    public Map<String, String> getUIDsForURLs(List<String> urls) throws SQLException {
        StringBuffer inBuf = new StringBuffer(GET_UIDS_WHERE_URL_IN_ARRAY_BASE);

        for (int i = 0; i < urls.size(); ++i) {
            String s = (String) urls.get(i);
            inBuf.append("'");
            inBuf.append(s);
            inBuf.append("'");
            if (i != urls.size() - 1) {
                inBuf.append(",");
            }
        }
        inBuf.append(");");
        Statement stmt = this.conn.createStatement();
        stmt.execute(inBuf.toString());
        Object[] urlArray = urls.toArray();
        Map urlUidMap = new HashMap(urls.size());
        ResultSet rs = stmt.getResultSet();
        while (rs.next()) {
            String url = rs.getString("obj_url");
            String uid = rs.getString("obj_uid");
            urlUidMap.put(url, uid);
        }
        stmt.close();
        return urlUidMap;
    }

    protected void setItemCount() throws SQLException {
        Statement stmt = this.conn.createStatement();
        stmt.execute("SELECT COUNT(*) AS \"count\" FROM GROUPDAV_OBJECTS");
        ResultSet rs = stmt.getResultSet();
        rs.next();
        this.itemCount = rs.getInt("count");
        rs.close();
        if (this.itemCount == 0) {
            this.itemCount = 100;
        }
    }
    public void insertVirtualObjects(String parentId, String[] vguid, String[] vid) throws SQLException {
        PreparedStatement stmt = this.conn.prepareStatement("insert into virtual_objects (virtual_guid ,virtual_id ,parent_id ) VALUES (?,?,?)");
        if (vguid.length == 0)
            return;
        
        for (int i = 0; i < vid.length; i++) {
            stmt.setString(1, vguid[i]);
            stmt.setString(2, vid[i]);
            stmt.setString(3, parentId);
            stmt.addBatch();
        }
        stmt.executeBatch();
        stmt.close();
    }
    public List<String> getVIDs(String parentId) throws SQLException {
        PreparedStatement stmt = this.conn.prepareStatement("SELECT virtual_id FROM virtual_objects WHERE parent_id = ?");
        stmt.setString(1, parentId);
        ResultSet rs = stmt.executeQuery();
        ArrayList<String> vids = new ArrayList();
        while(rs.next()) {
            vids.add(rs.getString("virtual_id"));
        }
        stmt.close();
        return vids;
    }
    public void deleteVirtualObjects(List<String> vids) throws SQLException {
        if (vids.size() == 0) {
            return;
        }
        PreparedStatement stmt = this.conn.prepareStatement("DELETE FROM virtual_objects WHERE virtual_id = ?");
        for(String vid: vids) {
            stmt.setString(1, vid);
            stmt.addBatch();
        }
        stmt.executeBatch();
        stmt.close();
    }

    public Map<String,String> getVirtualObjectMap() throws SQLException {
        HashMap<String,String> map = new HashMap();
        Statement stmt = this.conn.createStatement();
        stmt.execute("SELECT virtual_guid, parent_id FROM virtual_objects");
        ResultSet rs = stmt.getResultSet();
        while(rs.next()) {
            map.put(rs.getString("virtual_guid"),rs.getString("parent_id"));
        }
        stmt.close();
        return map;
    }
}
