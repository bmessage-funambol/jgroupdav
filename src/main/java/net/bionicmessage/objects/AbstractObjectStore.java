/* AbstractObjectStore.java
 * Copyright 2006-2010 Mathew McBride
 *
 * Redistribution and use in source and binary forms, with or without modification, are
 * permitted provided that the following conditions are met:
 *
 *    1. Redistributions of source code must retain the above copyright notice, this list of
 *       conditions and the following disclaimer.
 *
 *    2. Redistributions in binary form must reproduce the above copyright notice, this list
 *       of conditions and the following disclaimer in the documentation and/or other materials
 *       provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY Mathew McBride ``AS IS'' AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
 * FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 * ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * The views and conclusions contained in the software and documentation are those of the
 * authors and should not be interpreted as representing official policies, either expressed
 * or implied, of Mathew McBride.
 */
package net.bionicmessage.objects;

import com.funambol.common.pim.vcard.TokenMgrError;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.StringReader;
import java.net.URI;
import java.net.URL;
import java.net.URLEncoder;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;
import java.util.Set;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;
//import net.bionicmessage.groupdav.DAVServer;
import net.bionicmessage.groupdav.CardDAVServer;
import net.bionicmessage.groupdav.DAVServer;
import net.bionicmessage.groupdav.DAVServerFactory;
import net.bionicmessage.groupdav.GroupDAVObject;
import net.bionicmessage.utils.HTMLFormatter;
import net.bionicmessage.utils.QPDecode;
import net.fortuna.ical4j.data.CalendarBuilder;
import net.fortuna.ical4j.model.Calendar;

public abstract class AbstractObjectStore {

    DAVServer server = null;
    ObjectDatabase db = null;
    URI hostURI = null;
    public static int OPTION_I_AM_AN_EXPERT = 128;
    public static int OPTION_PURGE = 64;
    public static int OPTION_STOREDPASS = 8;
    public static int OPTION_SINGLELOG = 4;
    boolean expertmode = false;
    boolean purgemode = false;
    boolean singlelog = false;
    String obtrackdir = "";
    Properties props = null;
    File propfile = null;
    ArrayList<String> addedToStore = new ArrayList();
    ArrayList<String> updatedInStore = new ArrayList();
    ArrayList<String> deletedFromStore = new ArrayList();
    ArrayList<String> untouched = new ArrayList();
    ArrayList<String> addedToServer = new ArrayList();
    ArrayList<String> updatedOnServer = new ArrayList();
    ArrayList<String> deletedFromServer = new ArrayList();
    Hashtable<String, String> sources = new Hashtable();

    boolean addrMode = false;
    
    Map<String,String> virtualObjects = null;
    Logger log = null;
    FileHandler fh = null;
    int serverMode = 0;
    ArrayList<ObjectStoreException> exceptions = new ArrayList();
    String contentType = "text/calendar";

    protected AbstractObjectStore(String storedir, int options) {
        this.log = Logger.getLogger("groupdav.icalobjectstore-" + System.currentTimeMillis());
        int experttest = options & OPTION_I_AM_AN_EXPERT;
        if (experttest == OPTION_I_AM_AN_EXPERT) {
            this.expertmode = true;
        }
        int purgetest = options & OPTION_PURGE;
        if (purgetest == OPTION_PURGE) {
            this.purgemode = true;
        }
        int singletest = options & OPTION_SINGLELOG;
        if (singletest == OPTION_SINGLELOG) {
            this.singlelog = true;
        }

        this.obtrackdir = storedir + File.separatorChar + "obtrack" + File.separatorChar;
        this.db = new ObjectDatabase(this.obtrackdir);
        this.props = new Properties();
        this.propfile = new File(storedir + File.separatorChar + "vcalstoreprops");
        if (this.propfile.exists()) {
            try {
                FileInputStream propstream = new FileInputStream(this.propfile);
                this.props.load(propstream);
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
        try {
            long curTime = new Date().getTime();
            HTMLFormatter hf = new HTMLFormatter();
            if (this.singlelog) {
                this.fh = new FileHandler(storedir + File.separatorChar + "storelog.html");
            } else {
                this.fh = new FileHandler(storedir + File.separatorChar + "storelog-" + curTime + ".html");
            }
            this.fh.setFormatter(hf);
            this.log.addHandler(this.fh);
            this.fh.setLevel(Level.ALL);
            this.log.setLevel(Level.ALL);
            logStart();
        } catch (SecurityException ex) {
            ex.printStackTrace();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    public void loadSourcesFromProps() {
        Iterator listOfProps = this.props.keySet().iterator();
        while (listOfProps.hasNext()) {
            String key = (String) listOfProps.next();
            if (key.contains("store.source.")) {
                String loc = this.props.getProperty(key);
                key = key.replace("store.source.", "");
                setStore(key, loc);
            }
        }
    }

    public void setProperties(Properties p) {
        this.props = p;
    }

    public void setServer(String url) {
        if ((this.props.getProperty("store.server") == null) || (this.expertmode)) {
            this.props.setProperty("store.server", url);
        }
    }

    public void setStore(String name, String storeurl) {
        if (!(storeurl.substring(storeurl.length()).equals("/"))) {
            storeurl = storeurl + "/";
        }
        if ((this.props.getProperty("store.source." + name) == null) || (this.expertmode)) {
            this.props.setProperty("store.source." + name, storeurl);
        }
        this.sources.put(name, storeurl);
    }

    public void setServer(URI host, String b64cache)
            throws Exception {
        this.hostURI = host;
        //this.server = 
        this.server = DAVServerFactory.getServer(host,addrMode,b64cache);
        this.server.setServer(host.toString());
        this.server.setAuthenticationBasic(b64cache);
    }

    public void setServerCardDAV(URI host, String b64cache) throws Exception {
        this.hostURI = host;
        //this.server =
        this.server = new CardDAVServer();
        this.server.setServer(host.toString());
        this.server.setAuthenticationBasic(b64cache);
    }
    public void setServer(URI host, String user, String pass) throws Exception {
        this.hostURI = host;
        this.server = DAVServerFactory.getServer(host,addrMode,user,pass);
        //this.server = new GroupDAV2Server();
        this.server.setServer(host.toString());
        this.server.setAuthentication(user, pass);
    }

    public void setServerFromProperties() throws Exception {
        String user = this.props.getProperty("store.user");
        String password = this.props.getProperty("store.password");
        String srv = this.props.getProperty("store.server");
        this.hostURI = new URI(srv);
        this.server = DAVServerFactory.getServer(this.hostURI,addrMode,user,password);
        //this.server = new GroupDAV2Server();
        this.server.setServer(srv);
        this.server.setAuthentication(user, password);
    }

    protected String addFromServerToStore(String storeName, String url)
            throws Exception {
        return addFromServerToStore(storeName, url, this.addedToStore);
    }

    protected String addFromServerToStore(String storeName, String url, List<String> logList) throws Exception {
        URI pathURI = this.hostURI.resolve(url);
        GroupDAVObject obj = this.server.downloadSingleObject(pathURI.getPath());
        return addFromServerToStore(storeName, obj, logList);
    }

    abstract String addFromServerToStore(String paramString, GroupDAVObject paramGroupDAVObject, List<String> paramList)
            throws Exception;

    abstract void updateObjectFromServer(String paramString, GroupDAVObject paramGroupDAVObject)
            throws Exception;

    private void deleteFromStore(String uid)
            throws Exception {
        this.db.deleteObjectByUid(uid);
        this.deletedFromStore.add(uid);
    }

    public String addObject(String sourceName, String uid, String name, String contents, long dtstart, long dtend)
            throws Exception {
        URI sourceURI = new URI((String) this.sources.get(sourceName));
        /* The g_ stops eGroupware from parsing the UID as a number,
         * which it thinks must be an existing event
         */
        String fileName = encodeFileName(uid + ".ics");
        
        sourceURI = sourceURI.resolve(fileName);
        
        GroupDAVObject gbo = this.server.putObject(sourceURI.toASCIIString(), this.contentType, contents.getBytes());

        String srvuid = addFromServerToStore(sourceName, gbo.getLocation(), this.addedToServer);
        return srvuid;
    }

    protected void replaceObject(String storeName, String uid, String newServerContents)
            throws ObjectStoreException {
        try {
            String[] urletag = this.db.getUrlEtagForUid(uid);
            GroupDAVObject gbo = this.server.modifyObject(urletag[0], urletag[1], this.contentType, newServerContents.getBytes());
            this.db.deleteObjectByUid(uid);
            addFromServerToStore(storeName, urletag[0], this.updatedOnServer);
        } catch (Exception ex) {
            throw new ObjectStoreException(uid, ObjectStoreException.OP_DOWNLOAD_ADD, newServerContents.getBytes(), ex);
        }
    }

    public abstract String searchUids(String paramString, long paramLong1, long paramLong2) throws Exception;

    public void deleteObject(String uid) throws Exception {
        String[] url_etag = this.db.getUrlEtagForUid(uid);
        if (url_etag == null) {
            throw new Exception("Object with UID:" + uid + " not found");
        }
        boolean deleted = this.server.deleteObject(url_etag[0], url_etag[1]);
        this.db.deleteObjectByUid(uid);
        if (deleted) {
            this.deletedFromServer.add(uid);
        }
    }

    private Calendar constructCalendarObject(byte[] content)
            throws Exception {
        String ct = new String(content, "UTF-8");

        String debugct = ct.replace("\r", "xR").replace("\n", "xN");
        CalendarBuilder cbuild = new CalendarBuilder();
        StringReader sread = new StringReader(ct);
        Calendar cd = cbuild.build(sread);

        Calendar decoded = QPDecode.decodeQP(cd);
        return decoded;
    }

    public void startSync()
            throws Exception {
        this.log.info("Sync started....");
        if (this.purgemode) {
            this.db.purge();
        }
        this.db.open();
        Iterator sourceList = this.sources.keySet().iterator();
        while (sourceList.hasNext()) {
            String sourceName = (String) sourceList.next();
            String sourceLoc = (String) this.sources.get(sourceName);
            List toDownload = null;
            List toUpdate = null;
            Map<String, String> etags = this.server.listObjects(sourceLoc);
            Map<String, String> existingEtags = this.db.getUrlEtagMapForStore(sourceName);

            toDownload = new ArrayList(etags.size());
            toUpdate = new ArrayList(etags.size() / 10);
            for (Map.Entry<String, String> e : etags.entrySet()) {
                String url = e.getKey();
                String etag = e.getValue();
                URI objectURI = this.hostURI.resolve(url);
                url = objectURI.getPath().replace(" ","%20");
                String existingEtag = (String) existingEtags.get(url);
                if ((existingEtag == null) && (etag.length() != 0)) {
                    toDownload.add(url);
                } else if ((!(etag.equals(existingEtag))) && (etag.length() != 0)) {
                    toUpdate.add(url);
                    existingEtags.remove(url);
                } else if (existingEtag != null) {
                    existingEtags.remove(url);
                }
            }

            List<GroupDAVObject> downloadedObjs = this.server.getObjects(sourceLoc,toDownload);
            for (GroupDAVObject o : downloadedObjs) {
                try {
                    addFromServerToStore(sourceName, o, this.addedToStore);
                } catch (Exception e) {
                    ObjectStoreException ex = new ObjectStoreException(o.getLocation(),
                            ObjectStoreException.OP_ADD_TO_SERVER, o.getContent(), e);

                    this.exceptions.add(ex);
                    this.log.throwing(this.getClass().getName(), "beginSync", ex);
                } catch (TokenMgrError e) {
                    this.log.throwing(this.getClass().getName(), "beginSync", e);
                }
            }

            Map urluid = this.db.getUIDsForURLs(toUpdate);
            List<GroupDAVObject> updatedObjs = this.server.getObjects(sourceLoc,toUpdate);
            for (GroupDAVObject o : updatedObjs) {
                String uid = (String) urluid.get(o.getLocation());
                updateObjectFromServer(uid, o);
            }
            ArrayList deletedUrls = new ArrayList(existingEtags.keySet());
            Map<String, String> deletedUrlUid = this.db.getUIDsForURLs(deletedUrls);
            for (Map.Entry<String, String> e : deletedUrlUid.entrySet()) {
                String url = (String) e.getKey();
                String uid = (String) e.getValue();
                deleteFromStore(uid);
            }

        }
        this.virtualObjects = this.db.getVirtualObjectMap();
        this.log.info("Sync finished");
    }

    public ArrayList getUnModifiedUIDS() {
        return this.untouched;
    }

    /** @deprecated */
    public ArrayList getAddedToServerUIDS() {
        return this.addedToServer;
    }

    public ArrayList getAddedToStoreUIDS() {
        return this.addedToStore;
    }

    public ArrayList getDeletedFromServerUIDS() {
        return this.deletedFromServer;
    }

    public ArrayList getDeletedFromStoreUIDS() {
        return this.deletedFromStore;
    }

    public ArrayList getUpdatedInStoreUIDS() {
        return this.updatedInStore;
    }

    public ArrayList getUpdatedOnServerUIDS() {
        return this.updatedOnServer;
    }

    public ArrayList getUIDList()
            throws SQLException {
        return this.db.getAllUIDS();
    }

    public void close()
            throws Exception {
        this.fh.close();
        this.db.close();
    }

    protected void logStart() {
        this.log.info(super.getClass().getName() + " started in " + this.obtrackdir);
        this.log.info("Purge mode=" + this.purgemode);
        Iterator it = this.props.keySet().iterator();
        while (it.hasNext()) {
            String prop = (String) it.next();
            String key = this.props.getProperty(prop);
            this.log.info(prop + "=" + key);
        }
    }

    public List<ObjectStoreException> collectExceptions() {
        ArrayList collectedExceptions = this.exceptions;
        this.exceptions = new ArrayList();
        return collectedExceptions;
    }

    public void setContentType(String contentType) {
        this.contentType = contentType;
    }

    public String getContentType() {
        return this.contentType;
    }

    /**
     * This just replaces any characters in the string
     * which need to be encoded. For now, we only encode
     * a small set seen in the wild (i.e Synthesis on iOS)
     * @param fileName
     */
    private String encodeFileName(String fileName) {
        String f = fileName.replace(" ", "%20");
        f = f.replace(":","%3A");
        return f;
    }

    public void setAddrMode(boolean addrMode) {
        this.addrMode = addrMode;
    }

    public boolean isAddrMode() {
        return addrMode;
    }

}
