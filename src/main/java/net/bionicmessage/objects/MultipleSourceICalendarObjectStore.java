/*
 * MultipleSourceICalendarObjectStore.java
 *
 * Created on 26 August 2006, 14:56
 * Copyright (c) 2006-2010 Mathew McBride / "BionicMessage.net"
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"), 
 * to deal in the Software without restriction, including without limitation 
 * the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 * and/or sell copies of the Software, and to permit persons to whom the 
 * Software is furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in 
 * all copies or substantial portions of the Software.
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR 
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
 * IN THE SOFTWARE.
 *
 */
package net.bionicmessage.objects;

import java.io.PrintWriter;
import java.io.StringReader;
import java.io.StringWriter;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import net.bionicmessage.groupdav.CalDAVFormatter;
import net.bionicmessage.groupdav.GroupDAVObject;
import net.bionicmessage.groupdav.util.icalUtilities;
import net.bionicmessage.utils.QPDecode;
import net.fortuna.ical4j.data.CalendarBuilder;
import net.fortuna.ical4j.model.Calendar;
import net.fortuna.ical4j.model.Component;
import net.fortuna.ical4j.model.property.Summary;
import net.fortuna.ical4j.model.property.Uid;

public class MultipleSourceICalendarObjectStore extends AbstractObjectStore {

    protected List<String> neededComponentTypes;
    Hashtable<String, net.fortuna.ical4j.model.Calendar> objCache;
    protected java.util.Calendar jcal;

    public MultipleSourceICalendarObjectStore(String storedir, int options) {
        this(storedir, options, null);
    }

    @Override
    public void startSync() throws Exception {
        //this.server.setFormatter(new CalDAVFormatter());
        super.startSync();
    }

    public MultipleSourceICalendarObjectStore(String storedir, int options, List<String> neededCompTypes) {
        super(storedir, options);

        this.neededComponentTypes = null;

        this.objCache = new Hashtable();

        this.jcal = null;

        if (neededCompTypes != null) {
            this.neededComponentTypes = neededCompTypes;
        } else {
            this.neededComponentTypes = new ArrayList();
            this.neededComponentTypes.add("VEVENT");
            this.neededComponentTypes.add("VTODO");
        }
        setContentType("text/calendar");
        this.jcal = java.util.Calendar.getInstance();
    }

    String addFromServerToStore(String storeName, GroupDAVObject obj, List<String> logList) throws Exception {
        net.fortuna.ical4j.model.Calendar cd = constructCalendarObject(obj.getContent());
        Component vcal = null;
        if (this.neededComponentTypes.contains("VEVENT")) {
            vcal = cd.getComponents().getComponent("VEVENT");
        }
        if ((this.neededComponentTypes.contains("VTODO")) && (vcal == null)) {
            vcal = cd.getComponents().getComponent("VTODO");
        }
        if ((this.neededComponentTypes.contains("VJOURNAL")) && (vcal == null)) {
            vcal = cd.getComponents().getComponent("VJOURNAL");
        }
        if (vcal == null) {
            this.log.info("Input has no known format: " + obj.getContent());
            return null;
        }
        Uid uid = (Uid) vcal.getProperty("UID");
        this.objCache.put(uid.getValue(), cd);
        Summary summ = (Summary) vcal.getProperty("SUMMARY");
        Timestamp ds = null;
        Timestamp de = null;
        if (vcal.getName().equals("VEVENT")) {
            List fixed = icalUtilities.getDtStartEnd(vcal);
            ds = (Timestamp) fixed.get(0);
            this.jcal.setTime(ds);
            this.jcal.set(13, 0);
            ds.setTime(this.jcal.getTimeInMillis());
            de = (Timestamp) fixed.get(1);
            this.jcal.setTime(de);
            this.jcal.set(13, 0);
            de.setTime(this.jcal.getTimeInMillis());
            vcal = (Component) fixed.get(2);
        } else {
            ds = new Timestamp(0L);
            de = new Timestamp(0L);
        }
        /* We used to store the Calendar object itself but then
         * ical4j started slipping in non-serializable objects, so we will
         * parse again instead
         */
        String objectStr = cd.toString();
        this.db.createObject(storeName, uid.getValue(), obj.getLocation(), obj.getEtag(), summ.getValue().trim(), objectStr, ds, de);
        // Compile a list of recurrences
        List<String> recurrenceIds = RecurrenceManager.getDetatchedRecurrenceUids(cd);
        String[] virtual_guids = new String[recurrenceIds.size()];
        String[] vids = new String[recurrenceIds.size()];
        recurrenceIds.toArray(vids);
        for(int i=0; i<recurrenceIds.size(); i++) {
            virtual_guids[i] = uid.getValue().concat("-r").concat(vids[i]);
            logList.add(virtual_guids[i]);
        }
        this.db.insertVirtualObjects(uid.getValue(), virtual_guids, vids);
        logList.add(uid.getValue());
        return uid.getValue();
    }

    void updateObjectFromServer(String uid, GroupDAVObject obj) throws Exception {
        net.fortuna.ical4j.model.Calendar cd = constructCalendarObject(obj.getContent());
        Component vcal = cd.getComponents().getComponent("VEVENT");
        if (vcal == null) {
            vcal = cd.getComponents().getComponent("VTODO");
        }
        if (vcal == null) {
            vcal = cd.getComponents().getComponent("VJOURNAL");
        }
        Summary summ = (Summary) vcal.getProperty("SUMMARY");
        Timestamp dtstart = null;
        Timestamp dtend = null;
        if (vcal.getName().equals("VEVENT")) {
            List fixed = icalUtilities.getDtStartEnd(vcal);
            dtstart = (Timestamp) fixed.get(0);
            this.jcal.setTime(dtstart);
            this.jcal.set(13, 0);
            dtstart.setTime(this.jcal.getTimeInMillis());
            dtend = (Timestamp) fixed.get(1);
            this.jcal.setTime(dtend);
            this.jcal.set(13, 0);
            dtend.setTime(this.jcal.getTimeInMillis());
            vcal = (Component) fixed.get(2);
        } else {
            dtstart = new Timestamp(0L);
            dtend = new Timestamp(0L);
        }
        String objectStr = cd.toString();
        this.db.updateObject(uid, obj.getEtag(), summ.getValue(), dtstart, dtend, objectStr.toString());
        this.updatedInStore.add(uid);

        // Now find any deleted recurrences
        List<String> oldRids = this.db.getVIDs(uid);
        List<String> thisRids = RecurrenceManager.getDetatchedRecurrenceUids(cd);
        for(String s: thisRids) {
            if (oldRids.contains(s)) {
                oldRids.remove(s);
                this.updatedInStore.add(uid.concat("-r").concat(s));
            } else {
                this.addedToStore.add(uid.concat("-r").concat(s));
            }
        }
        for(String deleted: oldRids) {
            this.deletedFromStore.add(uid.concat("-r").concat(deleted));
        }
        this.db.deleteVirtualObjects(oldRids);
    }

    /** Search for existing UIDs with a specified name (summary), start time
     * and end time
     * @param name Object name (iCal SUMMARY)
     * @param dstime Start time in milliseconds
     * @param detime End time in milliseconds
     * @return A matching UID, or null if not found
     * @throws Exception
     */
    public String searchUids(String name, long dstime, long detime) throws Exception {
        Timestamp ms = new Timestamp(dstime);
        this.jcal.setTime(ms);
        this.jcal.set(13, 0);
        ms.setTime(this.jcal.getTimeInMillis());
        Timestamp me = new Timestamp(detime);
        this.jcal.setTime(me);
        this.jcal.set(13, 0);
        me.setTime(this.jcal.getTimeInMillis());
        return this.db.findUidForNameAndDate(name, ms, me);
    }

    public net.fortuna.ical4j.model.Calendar getObjectFromStore(String uid) throws ObjectStoreException {
        boolean isVirtualObject = this.virtualObjects.containsKey(uid);
        String rootObjectUid = (isVirtualObject) ? this.virtualObjects.get(uid) : uid;
        net.fortuna.ical4j.model.Calendar masterCal = null;
        if (this.objCache.get(rootObjectUid) != null) {
            this.log.fine("Object " + uid + "has been retrieved from store");
            masterCal = (net.fortuna.ical4j.model.Calendar) this.objCache.get(rootObjectUid);
        } else {
            try {
                String masterCalStr = (String) this.db.getObjectByUid(rootObjectUid);
                masterCal = constructCalendarObject(masterCalStr.getBytes("UTF-8"));
                if (masterCal == null) {
                    throw new ObjectStoreException(uid, ObjectStoreException.OP_NOTSPECIFIED);
                }
            } catch (Exception ex) {
                log.throwing(this.getClass().getName(), "getObjectFromStore", ex);
                throw new ObjectStoreException(uid, ObjectStoreException.OP_NOTSPECIFIED, new byte[0], ex);
            }
        }
        if (isVirtualObject) {
            String rId = uid.split("-r")[1];
            net.fortuna.ical4j.model.Calendar r = RecurrenceManager.obtainStandaloneRecurrence(masterCal, rId);
            return r;
        }
        return RecurrenceManager.obtainMasterEvent(masterCal);
    }

    private net.fortuna.ical4j.model.Calendar constructCalendarObject(byte[] content)
            throws Exception {
        String ct = new String(content, "UTF-8");

        String debugct = ct.replace("\r", "xR").replace("\n", "xN");
        CalendarBuilder cbuild = new CalendarBuilder();
        StringReader sread = new StringReader(ct);
        net.fortuna.ical4j.model.Calendar cd = cbuild.build(sread);

        net.fortuna.ical4j.model.Calendar decoded = QPDecode.decodeQP(cd);
        return decoded;
    }

    public net.fortuna.ical4j.model.Calendar updateCalendarObject(String storeName, String uid, Object replaced)
            throws ObjectStoreException {
        boolean isVirtualItem = this.virtualObjects.containsKey(uid);
        String rUID = (isVirtualItem) ? this.virtualObjects.get(uid) : uid;
        String newContent = null;
        if (isVirtualItem) {
            try {
                String recurrenceId = uid.split("-r")[1];
                String masterCal = this.db.getObjectByUid(rUID);
                net.fortuna.ical4j.model.Calendar master = constructCalendarObject(masterCal.getBytes("UTF-8"));
                Calendar merged =
                        RecurrenceManager.mergeRecurrenceChange(master, 
                        (net.fortuna.ical4j.model.Calendar)replaced,
                        rUID,recurrenceId);
                newContent = merged.toString();
            } catch (Exception e) {
                log.throwing(this.getClass().getName(), "updateCalendarObject", e);
                throw new ObjectStoreException(uid, ObjectStoreException.OP_NOTSPECIFIED, replaced.toString().getBytes(), e);
            }
        } else {
            newContent = replaced.toString();
        }
        super.replaceObject(storeName, rUID, newContent);
        return getObjectFromStore(uid);
    }

    public void printDebugReport() throws Exception {
        StringWriter sw = new StringWriter();
        PrintWriter lw = new PrintWriter(sw);
        ArrayList data = this.db.getAllData();
        for (int i = 0; i < data.size(); ++i) {
            Object[] dataarray = (Object[]) data.get(i);
            String uid = (String) dataarray[0];
            String url = (String) dataarray[1];
            if (url == null) {
                throw new Exception("URL for " + uid + " is null");
            }
            try {
                String source = (String) dataarray[3];
                String etag = (String) dataarray[2];
                String name = (String) dataarray[4];
                String content = (String) dataarray[7];
                Timestamp dtstart = (Timestamp) dataarray[5];
                Timestamp dtend = (Timestamp) dataarray[6];
                lw.println("----------");
                lw.println("SOURCE: " + source);
                lw.println("UID:" + uid);
                lw.println("URL:" + url);
                lw.println("ETAG:" + etag);
                lw.println("NAME:" + name);
                lw.println("DTSTART:" + dtstart.toString());
                lw.println("DTEND:" + dtend.toString());
                lw.println("DATA FOLLOWS:");
                lw.println(content);
                lw.println("----------");
            } catch (Exception e) {
                System.err.println("Error trying to get: " + uid);
                e.printStackTrace();
            }
        }
        lw.println("Objects added to store: ");
        String uid;
        int i;
        for (i = 0; i < this.addedToStore.size(); ++i) {
            uid = (String) this.addedToStore.get(i);
            lw.println("A:    " + uid);
        }
        lw.println("Objects updated from server: ");
        for (i = 0; i < this.updatedInStore.size(); ++i) {
            uid = (String) this.updatedInStore.get(i);
            lw.println("U:   " + uid);
        }
        lw.println("Objects deleted from store: ");
        for (i = 0; i < this.deletedFromStore.size(); ++i) {
            uid = (String) this.deletedFromStore.get(i);
            lw.println("D:    " + uid);
        }
        lw.println("Objects added to the server: ");
        for (i = 0; i < this.addedToServer.size(); ++i) {
            uid = (String) this.addedToServer.get(i);
            lw.println("SA:   " + uid);
        }
        lw.println("Objects merged to server: ");
        for (i = 0; i < this.updatedOnServer.size(); ++i) {
            uid = (String) this.updatedOnServer.get(i);
            lw.println("SM:   " + uid);
        }
        lw.println("Objects deleted from server: ");
        for (i = 0; i < this.deletedFromServer.size(); ++i) {
            uid = (String) this.deletedFromServer.get(i);
            lw.println("SD:   " + uid);
        }
        lw.flush();
        lw.close();
        this.log.info(sw.toString());
    }
}
