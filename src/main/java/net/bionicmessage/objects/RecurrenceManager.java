/*
 * Redistribution and use in source and binary forms, with or without modification, are
 * permitted provided that the following conditions are met:
 * 
 *    1. Redistributions of source code must retain the above copyright notice, this list of
 *       conditions and the following disclaimer.
 * 
 *    2. Redistributions in binary form must reproduce the above copyright notice, this list
 *       of conditions and the following disclaimer in the documentation and/or other materials
 *       provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY Mathew McBride ``AS IS'' AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
 * FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 * ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 * The views and conclusions contained in the software and documentation are those of the
 * authors and should not be interpreted as representing official policies, either expressed
 * or implied, of Mathew McBride.
 */
package net.bionicmessage.objects;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import net.fortuna.ical4j.model.Calendar;
import net.fortuna.ical4j.model.Component;
import net.fortuna.ical4j.model.ComponentList;
import net.fortuna.ical4j.model.Date;
import net.fortuna.ical4j.model.property.ExDate;
import net.fortuna.ical4j.model.property.RecurrenceId;
import net.fortuna.ical4j.model.property.Uid;

/**
 *
 * @author matt
 */
public class RecurrenceManager {

    public static List<String> getDetatchedRecurrenceUids(Calendar c) {
        ArrayList<String> uids = new ArrayList();
        ComponentList clist = (ComponentList) c.getComponents(Component.VEVENT);
        for (int i = 0; i < clist.size(); i++) {
            Component occurance = (Component) clist.get(i);
            Uid ui = (Uid) occurance.getProperty(Uid.UID);
            if (occurance.getProperty(RecurrenceId.RECURRENCE_ID) != null) {
                RecurrenceId rid = (RecurrenceId) occurance.getProperty(RecurrenceId.RECURRENCE_ID);
                uids.add(rid.getValue());
            }
        }
        return uids;
    }

    public static Calendar obtainStandaloneRecurrence(Calendar c, String recurrenceId) {
        ArrayList<String> uids = new ArrayList();
        ComponentList clist = (ComponentList) c.getComponents(Component.VEVENT);
        for (int i = 0; i < clist.size(); i++) {
            Component occurance = (Component) clist.get(i);
            Uid ui = (Uid) occurance.getProperty(Uid.UID);
            if (occurance.getProperty(RecurrenceId.RECURRENCE_ID) != null) {
                RecurrenceId rid = (RecurrenceId) occurance.getProperty(RecurrenceId.RECURRENCE_ID);
                // Correct the UID
                ui.setValue(ui.getValue()+"-r"+recurrenceId);
                occurance.getProperties().remove(ui);
                occurance.getProperties().add(ui);
                if (rid.getValue().equals(recurrenceId)) {
                    Calendar standalone = new Calendar();
                    standalone.getComponents().add(occurance);
                    return standalone;
                }
            }
        }
        return null;
    }

    public static ExDate getExdateForCalendar(Calendar c, ExDate exd) {
        ArrayList<Date> detatchedDates = new ArrayList();
        ComponentList clist = (ComponentList) c.getComponents(Component.VEVENT);
        for (int i = 0; i < clist.size(); i++) {
            Component occurance = (Component) clist.get(i);
            if (occurance.getProperty(RecurrenceId.RECURRENCE_ID) != null) {
                RecurrenceId rid = (RecurrenceId) occurance.getProperty(RecurrenceId.RECURRENCE_ID);
                detatchedDates.add(rid.getDate());
            }
        }
        for (Date d : detatchedDates) {
            exd.getDates().add(d);
        }
        return exd;
    }

    public static Calendar obtainMasterEvent(Calendar c) {
        ComponentList clist = (ComponentList) c.getComponents(Component.VEVENT);
        if (clist.size() == 0) {
            return c; // this is probably a task 
        }
        for (int i = 0; i < clist.size(); i++) {
            Component occurance = (Component) clist.get(i);
            if (occurance.getProperty(RecurrenceId.RECURRENCE_ID) == null) {
                Calendar cm = new Calendar();
                // Add the changed exdate
                ExDate exd = (ExDate) occurance.getProperty(ExDate.EXDATE);
                if (exd == null)
                    exd = new ExDate();
                else
                    occurance.getProperties().remove(exd);
                getExdateForCalendar(c, exd);
                occurance.getProperties().add(exd);
                cm.getComponents().add(occurance);
                return cm;
            }
        }
        return null;
    }
    public static Calendar mergeRecurrenceChange(Calendar master, Calendar recurrence, String masterUID, String recurrenceId) throws ParseException {
       ComponentList clist = (ComponentList) master.getComponents(Component.VEVENT);
       Calendar merged = master;
        for (int i = 0; i < clist.size(); i++) {
            Component occurance = (Component) clist.get(i);
            if (occurance.getProperty(RecurrenceId.RECURRENCE_ID) != null
                    && ((RecurrenceId)occurance.getProperty(RecurrenceId.RECURRENCE_ID)).getValue().equals(recurrenceId)) {
                merged.getComponents().remove(occurance);
                Component replacementOccurance = recurrence.getComponent(Component.VEVENT);
                // Delete the UID and replace it with the master
                Uid uid = (Uid) replacementOccurance.getProperty(Uid.UID);
                replacementOccurance.getProperties().remove(uid);
                uid.setValue(masterUID);
                replacementOccurance.getProperties().add(uid);

                RecurrenceId recurId = (RecurrenceId) replacementOccurance.getProperty(RecurrenceId.RECURRENCE_ID);
                if (recurId != null) {
                    replacementOccurance.getProperties().remove(recurId);
                }
                recurId = new RecurrenceId(recurrenceId);
                replacementOccurance.getProperties().add(recurId);
                merged.getComponents().add(replacementOccurance);
                break;
            }
        }
       return merged;
    }
}
