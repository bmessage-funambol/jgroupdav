/*
 * MultipleSourceVCardObjectStore.java
 *
 * Original file created on December 13, 2006, 10:44 PM
 * Copyright (C) 2006-2010 Mathew McBride
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"), 
 * to deal in the Software without restriction, including without limitation 
 * the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 * and/or sell copies of the Software, and to permit persons to whom the 
 * Software is furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in 
 * all copies or substantial portions of the Software.
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR 
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
 * IN THE SOFTWARE.
 */
package net.bionicmessage.objects;

import com.funambol.common.pim.contact.Contact;
import com.funambol.common.pim.contact.Name;
import com.funambol.common.pim.vcard.VcardParser;
import java.io.ByteArrayInputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;
import net.bionicmessage.groupdav.GroupDAVObject;
import net.bionicmessage.utils.vCardUtils;

/**
 * Storage and synchronization mechanism for vCard objects
 * @author matt
 */
public class MultipleSourceVCardObjectStore extends AbstractObjectStore {

    HashMap<String, Contact> objCache = new HashMap();

    public MultipleSourceVCardObjectStore(String storedir, int options) {
        super(storedir, options);
        this.contentType = "text/vcard";
    }

    String addFromServerToStore(String storeName, GroupDAVObject obj, List toAdd) throws Exception {
        Contact ct = constructContactObject(obj.getContent());
        String uid = ct.getUid();
        this.objCache.put(uid, ct);
        Name name = ct.getName();
        //String n = name.getDisplayName().getPropertyValueAsString();
        //if (name.getDisplayName().getPropertyValueAsString() == null) {
        String n = name.getFirstName().getPropertyValueAsString() + " " + name.getLastName().getPropertyValueAsString();
        //}
        n = n.trim();
        String objString = new String(obj.getContent());
        this.db.createObject(storeName, uid, obj.getLocation(), obj.getEtag(), n, objString, null, null);
        toAdd.add(uid);
        this.objCache.put(uid, ct);
        log.log(Level.FINE, "Item " + uid + " added");
        return uid;
    }

    @Override
    public void startSync() throws Exception {
        //this.server.setFormatter(new CardDAVFormatter());
        super.startSync();
    }


    public String addObject(String sourceName, String uid, String name, String contents)
            throws Exception {
        if (contents.indexOf("\nUID:") == -1) {
            int endLoc = contents.indexOf("END:VCARD");
            StringBuffer sb = new StringBuffer();
            sb.append(contents.substring(0, endLoc));
            sb.append("UID:");
            sb.append(uid);
            sb.append("\r\n");
            sb.append(contents.substring(endLoc));
            contents = sb.toString();
            sb = null;
        }
        // Remove empty lines to keep OGo happy
        String[] linesplit = contents.split("\r\n");
        StringBuffer newVcard = new StringBuffer();
        for (int i = 0; i < linesplit.length; i++) {
            String string = linesplit[i];
            if (string.lastIndexOf(':') != string.length()-1) {
                newVcard.append(string);
                newVcard.append("\r\n");
            }
        }
        /* URI sourceURI = this.hostURI.resolve((String) this.sources.get(sourceName));
        URI newURI = sourceURI.resolve(uid + ".vcf");

        GroupDAVObject gbo = this.server.putObject(newURI.getPath(), "text/vcard", contents.getBytes());
        return addFromServerToStore(sourceName, gbo.getLocation(), this.addedToServer); */
        return super.addObject(sourceName, uid, name, newVcard.toString(), serverMode, serverMode);
    }

    public int replaceObject(String storeName, String uid, String name, String contents)
            throws Exception {
        String[] urletag = this.db.getUrlEtagForUid(uid);
        GroupDAVObject gbo = this.server.modifyObject(urletag[0], urletag[1], "text/vcard", contents.getBytes());

        if (gbo == null) {
            return 403;
        }
        this.updatedOnServer.add(uid);
        this.db.deleteObjectByUid(uid);
        addFromServerToStore(storeName, urletag[0], this.updatedOnServer);
        return 0;
    }

    public int mergeObject(String newuid, String contents1, String contents2)
            throws Exception {
        return -1;
    }

    public Contact getObjectFromStore(String uid) throws ObjectStoreException {
        if (this.objCache.get(uid) != null) {
            this.log.fine("Object " + uid + "has been retrieved from store");
            Contact ct = (Contact) this.objCache.get(uid);
            return ct;
        }
        try {
            String contents = (String) this.db.getObjectByUid(uid);
            if (contents == null) {
                return null;
            }
            return constructContactObject(contents.getBytes("UTF-8"));
        } catch (Exception ex) {
            throw new ObjectStoreException(uid, ObjectStoreException.OP_DOWNLOAD_ADD, new byte[0], ex);
        }
    }

    private Contact constructContactObject(byte[] content)
            throws Exception {
        byte[] vCardData = content;
        /** If the 'BEGIN:VCARD' bit is in
         * lower case, we most likely have a vCard generated
         * by WebCit which we need to fix
         */
        if (vCardData[0] == 'b') {
            vCardData = vCardUtils.fixCitadelVCard(content);
        }
        ByteArrayInputStream bis = new ByteArrayInputStream(vCardData);
        VcardParser vcp = new VcardParser(bis, null, null);
        return vcp.vCard();
    }

    public void printDebugReport() throws Exception {
        StringWriter sw = new StringWriter();
        PrintWriter lw = new PrintWriter(sw);
        List data = this.db.getAllData();
        for (int i = 0; i < data.size(); ++i) {
            Object[] dataarray = (Object[]) data.get(i);
            String uid = (String) dataarray[0];
            String url = (String) dataarray[1];
            if (url == null) {
                throw new Exception("URL for " + uid + " is null");
            }
            String source = (String) dataarray[2];
            String etag = (String) dataarray[3];
            String name = (String) dataarray[4];
            String content = (String) dataarray[7];
            lw.println("----------");
            lw.println("UID:" + uid);
            lw.println("URL:" + url);
            lw.println("ETAG:" + etag);
            lw.println("NAME:" + name);
            lw.println("DATA FOLLOWS:");
            lw.println(content);
            lw.println("----------");
        }
        lw.println("Objects added to store: ");
        String uid;
        int i;
        for (i = 0; i < this.addedToStore.size(); ++i) {
            uid = (String) this.addedToStore.get(i);
            lw.println("A:    " + uid);
        }
        lw.println("Objects updated from server: ");
        for (i = 0; i < this.updatedInStore.size(); ++i) {
            uid = (String) this.updatedInStore.get(i);
            lw.println("U:   " + uid);
        }
        lw.println("Objects deleted from store: ");
        for (i = 0; i < this.deletedFromStore.size(); ++i) {
            uid = (String) this.deletedFromStore.get(i);
            lw.println("D:    " + uid);
        }
        lw.println("Objects added to the server: ");
        for (i = 0; i < this.addedToServer.size(); ++i) {
            uid = (String) this.addedToServer.get(i);
            lw.println("SA:   " + uid);
        }
        lw.println("Objects merged to server: ");
        for (i = 0; i < this.updatedOnServer.size(); ++i) {
            uid = (String) this.updatedOnServer.get(i);
            lw.println("SM:   " + uid);
        }
        lw.println("Objects deleted from server: ");
        for (i = 0; i < this.deletedFromServer.size(); ++i) {
            uid = (String) this.deletedFromServer.get(i);
            lw.println("SD:   " + uid);
        }
        lw.flush();
        lw.close();
        this.log.info(sw.toString());
    }

    private int getDownloaderParameter() {
        return 0;
    }

    void updateObjectFromServer(String uid, GroupDAVObject obj) throws Exception {
        Contact ct = constructContactObject(obj.getContent());
        Name name = ct.getName();
        this.objCache.put(uid, ct);
        String nameValue = name.getDisplayName().getPropertyValueAsString();
        if (nameValue == null) {
            nameValue = name.getFirstName() + " " + name.getLastName();
        }
        nameValue = nameValue.trim();
        String strContent = new String(obj.getContent());
        this.db.updateObject(uid, obj.getEtag(), nameValue, null, null, strContent);

        this.updatedInStore.add(uid);
    }

    public String searchUids(String name, long dstime, long detime) throws Exception {
        return this.db.findUidForNameAndDate(name, null, null);
    }
}
