/*
 * Redistribution and use in source and binary forms, with or without modification, are
 * permitted provided that the following conditions are met:
 * 
 *    1. Redistributions of source code must retain the above copyright notice, this list of
 *       conditions and the following disclaimer.
 * 
 *    2. Redistributions in binary form must reproduce the above copyright notice, this list
 *       of conditions and the following disclaimer in the documentation and/or other materials
 *       provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY Mathew McBride ``AS IS'' AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
 * FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 * ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 * The views and conclusions contained in the software and documentation are those of the
 * authors and should not be interpreted as representing official policies, either expressed
 * or implied, of Mathew McBride.
 */

package net.bionicmessage.groupdav;

import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import net.bionicmessage.groupdav.http.HttpClientUtil;
import net.bionicmessage.groupdav.http.HttpPropfind;
import net.bionicmessage.groupdav.http.HttpReport;
import org.apache.http.HttpHost;
import org.apache.http.HttpResponse;
import org.apache.http.auth.AuthenticationException;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.HttpClient;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.auth.BasicScheme;
import org.apache.http.util.EntityUtils;
import org.xml.sax.InputSource;

/**
 * Implements methods for CardDAV Servers
 * @author matt
 */
public class CardDAVServer extends GroupDAV2Server {

    protected static final String CARDDAV_ETAGREPORT = "<?xml version=\"1.0\" encoding=\"utf-8\" ?>\r\n "
            + "<C:addressbook-query xmlns:D=\"DAV:\" xmlns:C=\"urn:ietf:params:xml:ns:carddav\">"
            + "\t<D:prop>"
            + "\t\t<D:getetag/><D:resourcetype/>"
            + "\t</D:prop>"
            + "<C:filter></C:filter>"
            + "</C:addressbook-query>";
    
    public CardDAVServer() {
        super();
        formatter = new CardDAVFormatter();
    }

    @Override
    public Map<String, String> listObjects(String url) throws Exception {
        URI pathURI = this.serverURI.resolve(url);

        HttpReport query = new HttpReport(pathURI);
        query.setHeader(basicAuth.authenticate(davCreds, query));
        
        StringEntity he = new StringEntity(CARDDAV_ETAGREPORT);
        he.setContentType("text/xml");
        query.setEntity(he);
        HttpResponse hr = this.httpClient.execute(this.httpHost, query, this.httpContext);
        InputSource is = new InputSource(hr.getEntity().getContent());
        this.handler = new SAXDAVHandler(this, is);
        while (!(this.ready));
        this.ready = false;
        EntityUtils.consume(he);

        Map<String,String> etags = this.handler.getEtagMap();
        ArrayList<String> collectionURLs = new ArrayList();
        Set<Entry<String,String>> entrySet = etags.entrySet();
        Iterator<Entry<String,String>> entrySetIterator = entrySet.iterator();
        while(entrySetIterator.hasNext()) {
            Entry<String,String> entry = entrySetIterator.next();
            String itemURL = entry.getKey();
            if (itemURL.endsWith("/")) {
                // This is a collection
                collectionURLs.add(itemURL);
                entrySetIterator.remove();
            }
        }

        for(String anotherCollection: collectionURLs) {
            Map<String,String> subCollection = listObjects(anotherCollection);
            etags.putAll(subCollection);
        }

        return etags;
    }

    @Override
    public boolean doesSupportMultiget(String path) throws IOException, AuthenticationException {
        return true;
    }
  
}
