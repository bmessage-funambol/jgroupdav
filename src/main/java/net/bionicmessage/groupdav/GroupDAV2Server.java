/* GroupDAV2Server.java
 * Copyright 2010 Mathew McBride
 *
 * Redistribution and use in source and binary forms, with or without modification, are
 * permitted provided that the following conditions are met:
 *
 *    1. Redistributions of source code must retain the above copyright notice, this list of
 *       conditions and the following disclaimer.
 *
 *    2. Redistributions in binary form must reproduce the above copyright notice, this list
 *       of conditions and the following disclaimer in the documentation and/or other materials
 *       provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY Mathew McBride ``AS IS'' AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
 * FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 * ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * The views and conclusions contained in the software and documentation are those of the
 * authors and should not be interpreted as representing official policies, either expressed
 * or implied, of Mathew McBride.
 */
package net.bionicmessage.groupdav;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Stack;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.bionicmessage.extutils.Base64;
import net.bionicmessage.groupdav.http.HttpClientUtil;
import net.bionicmessage.groupdav.http.HttpPropfind;
import net.bionicmessage.groupdav.http.HttpReport;
import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpHost;
import org.apache.http.HttpResponse;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.AuthenticationException;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.AuthCache;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpOptions;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.client.protocol.ClientContext;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.auth.BasicScheme;
import org.apache.http.impl.client.BasicAuthCache;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.util.EntityUtils;
import org.xml.sax.InputSource;

public class GroupDAV2Server
        implements DAVServer, IDAVHandler {

    /* private static final String DAV_PROPFIND = "<?xml version=\"1.0\" " +
            "encoding=\"utf-8\"?>\n<propfind xmlns=\"DAV:\"\n xmlns:I=\"urn:ietf:params:xml:ns:caldav\"\n" +
            " xmlns:C=\"urn:ietf:params:xml:ns:carddav\"\n" +
            " xmlns:G=\"http://groupdav.org/\"\n" +
            " xmlns:A=\"http://calendarserver.org/ns/\">\n\t" +
            "<prop>\n\t\t<displayname/>\n\t\t<resourcetype/>\n\t\t" +
            "<getetag/>\n\t\t<getcontenttype/>\n\t\t<current-user-privilege-set/>" +
            "\n\t\t<G:component-set/>\n\t\t<I:supported-calendar-component-set/>" +
            "\n\t\t<I:supported-calendar-data/>\n\t\t<I:calendar-description/>" +
            "\n\t\t<C:supported-address-data/>\n\t\t<C:addressbook-description/>" +
            "\n\t\t<A:getctag/>\n\t</prop>\n</propfind>\n";  */
    private static final String DAV_PROPFIND = "<?xml version=\"1.0\" encoding=\"utf-8\"?>"
            + "<propfind xmlns=\"DAV:\"><prop><getetag/></prop></propfind>"; 

    private Logger logger = null;
    protected URI serverURI = null;
    protected DefaultHttpClient httpClient = null;
    protected BasicHttpContext httpContext = null;
    protected HttpHost httpHost = null;
    protected SAXDAVHandler handler = null;
    boolean ready = false;
    private Exception exception = null;
    protected DAVReportFormatter formatter = null;

    protected BasicScheme basicAuth = null;
    protected UsernamePasswordCredentials davCreds = null;

    
    public GroupDAV2Server() {
        this.httpClient = HttpClientUtil.createSSLCapableClient();
        this.httpContext = new BasicHttpContext();
    }

    public Map<String, String> listObjects(String url) throws Exception {
        URI pathURI = this.serverURI.resolve(url);

        HttpPropfind pf = new HttpPropfind(pathURI);
        StringEntity he = new StringEntity(DAV_PROPFIND);
        pf.setHeader("Depth","1");

        he.setContentType("text/xml");
        pf.setEntity(he);
        HttpResponse hr = this.httpClient.execute(this.httpHost, pf, this.httpContext);
        InputSource is = new InputSource(hr.getEntity().getContent());
        this.handler = new SAXDAVHandler(this, is);
        while (!(this.ready));
        this.ready = false;


        EntityUtils.consume(hr.getEntity());
        
        return this.handler.getEtagMap();
    }

    public List<GroupDAVObject> getObjects(String baseURL, final List<String> urls) throws Exception {
        // Do we support multiget?
        boolean supportMultiget = doesSupportMultiget(baseURL);
        ArrayList<GroupDAVObject> downloaded = new ArrayList(urls.size());
        ArrayList<String> downloadBatch = getBatch(urls, 500);
        while (!downloadBatch.isEmpty()) {
            if (supportMultiget) {
                try {
                    List<GroupDAVObject> batch = downloadMultipleObjects(baseURL, downloadBatch);
                    if (batch != null) {
                        downloaded.addAll(batch);
                    } else {
                        // Multiget failed, attempt a single download for the entire batch
                        for (String singleURL : downloadBatch) {
                            GroupDAVObject gdo = this.downloadSingleObject(singleURL);
                            downloaded.add(gdo);
                        }
                    }
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            } else {
                for (String s : downloadBatch) {
                    try {
                        GroupDAVObject gdo = downloadSingleObject(s);
                        downloaded.add(gdo);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
            downloadBatch = getBatch(urls, 500);
        }
        
        return downloaded;
    }

    public GroupDAVObject modifyObject(String url, String etag, String ctype, byte[] newContents) throws IOException, AuthenticationException {
        URI pathURI = this.serverURI.resolve(url);

        HttpResponse hr = null;

        try {
            HttpPut put = new HttpPut(pathURI);
            put.addHeader("If-Match", etag);
            put.addHeader(basicAuth.authenticate(davCreds, put, httpContext));


            ByteArrayEntity entity = new ByteArrayEntity(newContents);
            entity.setContentType(ctype + ";charset=UTF-8");
            //entity.setContentEncoding("UTF-8");
            put.setEntity(entity);


            hr = this.httpClient.execute(this.httpHost, put, this.httpContext);

            if (hr.getStatusLine().getStatusCode() != 204) {
                return null;
            }
            GroupDAVObject gbo = new GroupDAVObject();
            gbo.setStatus(hr.getStatusLine().getStatusCode());
            return gbo;
        } catch (IOException ex) {
            throw (ex);
        } finally {
            if (hr != null) {
                EntityUtils.consume(hr.getEntity());
            }
        }

    }

    public GroupDAVObject putObject(String url, String ctype, byte[] content) throws IOException, AuthenticationException  {
        URI pathURI = this.serverURI.resolve(url);
        //pathURI = pathURI.resolve("new.ics");
        HttpResponse hr = null;
        GroupDAVObject obj = null;

        try {
            HttpPut put = new HttpPut(pathURI);
            put.addHeader(basicAuth.authenticate(davCreds, put, httpContext));
            put.addHeader("Accept", "*/*");
            put.addHeader("If-None-Match", "*");


            ByteArrayEntity entity = new ByteArrayEntity(content);
            entity.setContentType(ctype + ";charset=UTF-8");

            put.setEntity(entity);
            hr = this.httpClient.execute(this.httpHost, put, this.httpContext);
            
            obj = new GroupDAVObject();
            Header loc = hr.getFirstHeader("Location");
            if (loc != null) {
                obj.setLocation(loc.getValue());
            } else {
                obj.setLocation(url);
            }
        } catch (IOException ex) {
            throw (ex);
        } finally {
            if (hr != null) {
                EntityUtils.consume(hr.getEntity());
            }
        }
        return obj;
    }

    public boolean deleteObject(String url, String etag) throws IOException, AuthenticationException {
        URI pathURI = this.serverURI.resolve(url);

        HttpResponse hr = null;
        try {
            HttpDelete delete = new HttpDelete(pathURI);
            delete.addHeader(basicAuth.authenticate(davCreds, delete, this.httpContext));
            delete.addHeader("If-Match", etag);

            hr = this.httpClient.execute(this.httpHost, delete, this.httpContext);




            return (hr.getStatusLine().getStatusCode() == 204);
        } finally {
            EntityUtils.consume(hr.getEntity());
        }
    }

    public void setProperty(String name, Object value) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public void setAuthenticationBasic(String b64) {
        byte[] decoded = Base64.decode(b64);
        String up = new String(decoded);
        AuthScope as = new AuthScope(this.serverURI.getHost(), this.serverURI.getPort());
        davCreds = new UsernamePasswordCredentials(up);

        HttpHost targetHost =
                new HttpHost(this.serverURI.getHost(), this.serverURI.getPort());
        this.httpClient.getCredentialsProvider().setCredentials(as, davCreds);
        AuthCache authCache = new BasicAuthCache();
        basicAuth = new BasicScheme();
        authCache.put(targetHost, basicAuth);

// Add AuthCache to the execution context
        this.httpContext.setAttribute(ClientContext.AUTH_CACHE, authCache);
    }

    public void setAuthentication(String user, String pass) {
        AuthScope as = new AuthScope(this.serverURI.getHost(), this.serverURI.getPort());
        davCreds = new UsernamePasswordCredentials(user, pass);

        HttpHost targetHost =
                new HttpHost(this.serverURI.getHost(), this.serverURI.getPort());
        this.httpClient.getCredentialsProvider().setCredentials(as, davCreds);
        AuthCache authCache = new BasicAuthCache();
        basicAuth = new BasicScheme();
        authCache.put(targetHost, basicAuth);

// Add AuthCache to the execution context
        this.httpContext.setAttribute(ClientContext.AUTH_CACHE, authCache);
        
        this.httpClient.getCredentialsProvider().setCredentials(as, davCreds);
    }

    public void setServer(String server) throws URISyntaxException {
        this.serverURI = new URI(server).parseServerAuthority();
        int port = 80;
        if (this.serverURI.getPort() != -1) {
            port = this.serverURI.getPort();
        }
        if (("https".equals(this.serverURI.getScheme())) && (port == 80)) {
            port = 443;
        }

        this.httpHost = new HttpHost(this.serverURI.getHost(), port, this.serverURI.getScheme());
        AuthCache authCache = new BasicAuthCache();

        basicAuth = new BasicScheme();
        authCache.put(this.httpHost, basicAuth);

        BasicHttpContext localcontext = new BasicHttpContext();
        localcontext.setAttribute(ClientContext.AUTH_CACHE, authCache);
    }

    public boolean getReady() {
        return this.ready;
    }

    public void setReady(boolean ready) {
        this.ready = ready;
    }

    public void setRetException(Exception e) {
        this.exception = e;
    }

    protected ArrayList<String> getBatch(List<String> collection, int size) {
        ArrayList batch = new ArrayList(size);
        int i = 0;
        Iterator iterator = collection.iterator();
        while ((iterator.hasNext()) && (i != size)) {
            String url = (String) iterator.next();
            batch.add(url);
            iterator.remove();
            ++i;
        }
        return batch;
    }
  
    public GroupDAVObject downloadSingleObject(String url) throws Exception {
        String p = url.replace(" ","%20");
        URI pathURI = this.serverURI.resolve(p);

        HttpResponse hr = null;
        HttpEntity he = null;
        try {
            HttpGet hg = new HttpGet(pathURI);
            hg.setHeader(basicAuth.authenticate(davCreds, hg, httpContext));
            hg.setHeader("Accept", "text/*");
            hg.setHeader("Content-Length", "0");

            hr = this.httpClient.execute(this.httpHost, hg, this.httpContext);
            he = hr.getEntity();
            byte[] data = EntityUtils.toByteArray(he);
            Header etag = hr.getFirstHeader("ETag");
            Header loc = hr.getFirstHeader("Location");
            GroupDAVObject gdo = new GroupDAVObject();
            if (loc != null) {
                URI newLocation = this.serverURI.resolve(loc.getValue());
                gdo.setLocation(newLocation.getPath());
            } else {
                String esc = pathURI.getPath().replace(" ","%20");
                gdo.setLocation(esc);
            }
            gdo.setEtag(etag.getValue());
            gdo.setContent(data);
            return gdo;
        } finally {
            if (he != null)
                EntityUtils.consume(he);
        }


    }


    public List<GroupDAVObject> downloadMultipleObjects(String url, List<String> urls) throws Exception {
        URI pathURI = this.serverURI.resolve(url);
        
        HttpReport hr = new HttpReport(pathURI);
        hr.addHeader(basicAuth.authenticate(davCreds, hr,httpContext));
        hr.setHeader("Depth", "1");
        
        ByteArrayEntity entity = new ByteArrayEntity(formatter.formatReport(urls));
        entity.setContentType("text/xml;charset=UTF-8");
        //entity.setContentEncoding("UTF-8");
        hr.setEntity(entity);
        // Send DAV Report
        HttpResponse hresponse = this.httpClient.execute(this.httpHost, hr, this.httpContext);
        if (hresponse.getStatusLine().getStatusCode() >= 500) {
            EntityUtils.consume(hresponse.getEntity());
            return null;
        }
        InputSource is = new InputSource(hresponse.getEntity().getContent());
        this.handler = new SAXDAVHandler(this, is);
        while (!(this.ready));
        this.ready = false;
        EntityUtils.consume(hresponse.getEntity());

        Stack<String> contents = this.handler.getContents();
        Stack<String> objectURLs = this.handler.getHrefs();
        Stack<String> etags = this.handler.getEtags();
        List<GroupDAVObject> objects = new ArrayList(etags.size());
        while(!objectURLs.empty()) {
            String objectURL = objectURLs.pop();
            String content = contents.pop();
            String etag = etags.pop();
            GroupDAVObject gdo = new GroupDAVObject();
            gdo.setContent(content.getBytes());
            gdo.setEtag(etag);
            gdo.setLocation(objectURL);
            objects.add(gdo);
        }
        return objects;
    }

    public void setFormatter(DAVReportFormatter formatter) {
        this.formatter = formatter;
    }

    public boolean doesSupportMultiget(String path) throws IOException, AuthenticationException {
        boolean supportMultiget = false;
        URI pathURI = this.serverURI.resolve(path);
        
        HttpOptions hr = new HttpOptions(pathURI);
        hr.addHeader(basicAuth.authenticate(davCreds, hr));
        hr.setHeader("Depth","1");
        
        HttpResponse response = this.httpClient.execute(this.httpHost, hr, this.httpContext);
        Header optionsHeader = response.getFirstHeader("DAV");
        if (optionsHeader != null) {
            String supported = optionsHeader.getValue();
            if (supported.contains("addressbook") ||
                    supported.contains("calendar-access")) {
                supportMultiget = true;
            }
        }

        EntityUtils.consume(response.getEntity());
        
        return supportMultiget;
    }
    
}
