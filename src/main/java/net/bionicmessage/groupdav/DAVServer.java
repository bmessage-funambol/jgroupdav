/*
 * DAVServer.java
 * Copyright 2010 Mathew McBride
 *
 * Redistribution and use in source and binary forms, with or without modification, are
 * permitted provided that the following conditions are met:
 *
 *    1. Redistributions of source code must retain the above copyright notice, this list of
 *       conditions and the following disclaimer.
 *
 *    2. Redistributions in binary form must reproduce the above copyright notice, this list
 *       of conditions and the following disclaimer in the documentation and/or other materials
 *       provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY Mathew McBride ``AS IS'' AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
 * FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 * ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * The views and conclusions contained in the software and documentation are those of the
 * authors and should not be interpreted as representing official policies, either expressed
 * or implied, of Mathew McBride.
 */
package net.bionicmessage.groupdav;

import java.util.List;
import java.util.Map;

public abstract interface DAVServer {

    public abstract void setServer(String paramString)
            throws Exception;

    public abstract void setProperty(String paramString, Object paramObject);

    public abstract void setAuthenticationBasic(String paramString);

    public abstract void setAuthentication(String paramString1, String paramString2);

    public abstract Map<String, String> listObjects(String paramString)
            throws Exception;

    public abstract List<GroupDAVObject> getObjects(String baseURL, List<String> paramList) throws Exception;

    public abstract GroupDAVObject modifyObject(String paramString1, String paramString2, String paramString3, byte[] paramArrayOfByte)
            throws Exception;

    public abstract GroupDAVObject putObject(String paramString1, String paramString2, byte[] paramArrayOfByte)
            throws Exception;

    public abstract boolean deleteObject(String paramString1, String paramString2)
            throws Exception;

    public abstract GroupDAVObject downloadSingleObject(String paramString)
            throws Exception;
}
