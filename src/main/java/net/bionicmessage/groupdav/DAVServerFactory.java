/* DAVServerFactory.java
 * Copyright 2010 Mathew McBride
 * 
 * Redistribution and use in source and binary forms, with or without modification, are
 * permitted provided that the following conditions are met:
 *
 *    1. Redistributions of source code must retain the above copyright notice, this list of
 *       conditions and the following disclaimer.
 *
 *    2. Redistributions in binary form must reproduce the above copyright notice, this list
 *       of conditions and the following disclaimer in the documentation and/or other materials
 *       provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY Mathew McBride ``AS IS'' AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
 * FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 * ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * The views and conclusions contained in the software and documentation are those of the
 * authors and should not be interpreted as representing official policies, either expressed
 * or implied, of Mathew McBride.
 */
package net.bionicmessage.groupdav;

import java.net.URI;
import net.bionicmessage.groupdav.http.HttpClientUtil;
import org.apache.commons.codec.binary.Base64;
import org.apache.http.Header;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpOptions;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.CoreProtocolPNames;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.util.EntityUtils;

public class DAVServerFactory {

    public static DAVServer getServer(URI host, boolean isAddr, String user, String pass)
            throws Exception {
        String encoded = user.concat(":").concat(pass);
        String b64 = new String(Base64.encodeBase64(encoded.getBytes()));
        return getServer(host, isAddr, b64);
    }
    public static DAVServer getServer(URI host, boolean isAddr, String b64data)
            throws Exception {
        DAVServer srv = null;
        HttpOptions opt = new HttpOptions(host);
        opt.setHeader("Authorization", "Basic "+b64data);
        DefaultHttpClient httpClient = HttpClientUtil.createSSLCapableClient();
        BasicHttpContext ctx = new BasicHttpContext();
        httpClient.getParams().setParameter(CoreProtocolPNames.USER_AGENT,
                HttpClientUtil.USER_AGENT);
        HttpResponse response = httpClient.execute(opt);

        boolean hasCardDAV = false;
        boolean hasCalDAV = false;
        Header[] DAVHeaders = response.getHeaders("DAV");
        for (int i = 0; i < DAVHeaders.length; i++) {
            Header header = DAVHeaders[i];
            if (isAddr && header.getValue().contains("addressbook")) {
                hasCardDAV = true;
            } else if (header.getValue().contains("calendar-access")) {
                hasCalDAV = true;
            }
        }
        EntityUtils.consume(response.getEntity());
        if (isAddr && hasCardDAV) {
            srv = new CardDAVServer();
        } else if (!isAddr && hasCalDAV) {
            srv = new CalDAVServer();
            ((CalDAVServer) srv).setFormatter(new CalDAVFormatter());
        } else {
            srv = new GroupDAV2Server();
            ((GroupDAV2Server) srv).setFormatter(new CalDAVFormatter());
        }
        return srv;
    }
}
