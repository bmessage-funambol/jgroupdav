/*
 * HTMLFormatter.java
 *
 * Created on 3 September 2006, 10:45
 *
 * Copyright (C) 2006 Mathew McBride 
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"), 
 * to deal in the Software without restriction, including without limitation 
 * the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 * and/or sell copies of the Software, and to permit persons to whom the 
 * Software is furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in 
 * all copies or substantial portions of the Software.
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR 
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
 * IN THE SOFTWARE.
 */
package net.bionicmessage.utils;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.logging.Formatter;
import java.util.logging.LogRecord;

/**
 *
 * @author matt
 */
public class HTMLFormatter extends Formatter {

    String LASTCLASS = "";
    String LASTMETHOD = "";

    /** Creates a new instance of HTMLFormatter */
    public HTMLFormatter() {
    }

    public String format(LogRecord record) {
        StringBuffer records = new StringBuffer();
        if (LASTCLASS.equals(record.getSourceClassName())) {
            records.append("<pre>");
            if (LASTMETHOD.equals(record.getSourceMethodName())) {

                records.append(addMessage(record.getMessage()));
                if (record.getThrown() != null) {
                    Throwable throwed = record.getThrown();
                    records.append(addThrowed(throwed));
                }
            } else {
                records.append(addMethodName(record.getSourceMethodName()));
                records.append(addMessage(record.getMessage()));
                if (record.getThrown() != null) {
                    Throwable throwed = record.getThrown();
                    records.append(addThrowed(throwed));
                }
            }
            records.append("</pre>");
        } else {
            LASTCLASS = record.getSourceClassName();
            LASTMETHOD = record.getSourceMethodName();
            records.append("<p><h2>");
            records.append(record.getSourceClassName());
            records.append(":");
            records.append(System.currentTimeMillis());
            records.append("</h2></p>");
            records.append(addMethodName(record.getSourceMethodName()));
            records.append("<pre>");
            if (record.getThrown() != null) {
                records.append(addThrowed(record.getThrown()));
            } else {
                records.append(addMessage(record.getMessage()));
            }
            records.append("</pre>");
        }
        return records.toString();
    }

    private String addMessage(String msg) {
        msg = msg.replace("<", "&lt;");
        msg = msg.replace(">", "&gt;");
        msg = msg.replace(":", "&#58;");
        StringBuffer records = new StringBuffer();
        records.append("<p><pre>");
        records.append(msg);
        records.append("</pre></p>");
        return records.toString();
    }

    private String addThrowed(Throwable ex) {
        try {
            StringBuffer records = new StringBuffer();
            StringWriter wr = new StringWriter();
            PrintWriter pw = new PrintWriter(wr);
            ex.printStackTrace(pw);
            pw.close();
            records.append(wr.toString());
            String msg = wr.toString();
            msg = msg.replace("<", "&lt;");
            msg = msg.replace(">", "&gt;");
            msg = msg.replace(":", "&#58;");
            return msg;

        } catch (Exception exp) {
            exp.printStackTrace();
        }
        return null;
    }

    private String addMethodName(String msg) {
        StringBuffer records = new StringBuffer();
        records.append("<p><h3>");
        records.append(msg);
        records.append(":");
        records.append(System.currentTimeMillis());
        records.append("</h3></p>");
        return records.toString();
    }
}
