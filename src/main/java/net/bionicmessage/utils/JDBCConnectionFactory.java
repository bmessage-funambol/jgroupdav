/*
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"), 
 * to deal in the Software without restriction, including without limitation 
 * the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 * and/or sell copies of the Software, and to permit persons to whom the 
 * Software is furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in 
 * all copies or substantial portions of the Software.
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR 
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
 * IN THE SOFTWARE.
 */
package net.bionicmessage.utils;

import java.util.Hashtable;
import java.sql.Connection;
import java.sql.SQLException;

/**
 * A 'factory' singleton-type thingy that ensures only ONE connection to a JDBC
 * resource exists
 * @author matt
 */
public class JDBCConnectionFactory {

    private static JDBCConnectionFactory instance =
            null;
    private static Hashtable<String, Connection> connectionTable = null;

    private JDBCConnectionFactory() {
        connectionTable = new Hashtable();
    }

    public static JDBCConnectionFactory getInstance() {
        if (instance == null) {
            return new JDBCConnectionFactory();
        }
        return instance;
    }

    public static Connection getConnection(String jdbcID) throws SQLException {
        if (connectionTable.get(jdbcID) != null) {
            return connectionTable.get(jdbcID);
        }
        // Else, create one
        Connection conn = java.sql.DriverManager.getConnection(jdbcID);
        connectionTable.put(jdbcID, conn);
        return conn;
    }

    public static void removeConnection(String jdbcID) {
        connectionTable.remove(jdbcID);
    }

    public static boolean doesConnectionExist(String jdbcID) {
        if (connectionTable.get(jdbcID) != null) {
            return true;
        }
        return false;
    }
}
