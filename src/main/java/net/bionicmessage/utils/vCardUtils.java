/*
 * vCardUtils.java
 *
 * Created 22 June 2012
 * Copyright (C) 2006-2012 Mathew McBride
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */
package net.bionicmessage.utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.StringReader;
import java.io.UnsupportedEncodingException;

/**
 * Utilities for manipulating vCards
 * @author matt
 */
public class vCardUtils {

    /**
     * Fixes vCards that come from Citadel, which may have lower case parameters
     * @param in the original vCard text
     * @return a byte array containing the fixed vCard
     * @throws UnsupportedEncodingException
     * @throws IOException
     */
    public static byte[] fixCitadelVCard(byte[] in) throws UnsupportedEncodingException, IOException {
        StringReader sr = new StringReader(new String(in, "UTF-8"));
        BufferedReader br = new BufferedReader(sr);
        String line = null;
        StringBuilder fixed = new StringBuilder();
        while ((line = br.readLine()) != null) {
            int delimiter = line.indexOf(':');
            if (delimiter > 0) {
                String key = line.substring(0, delimiter);
                String val = line.substring(delimiter, line.length());
                fixed.append(key.toUpperCase());
                fixed.append(val);
            } else {
                fixed.append(line);
            }
            fixed.append("\r\n");
        }
        return fixed.toString().getBytes("UTF-8");
    }
}
