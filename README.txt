GroupDAV Client and Object Store Framework for Java
----------------------------------------------------
This is a set of classes incorporating the GroupDAV client class and
an object store for Java, designed to allow rapid development of anything
needing access to data from a GroupDAV server.

The store is implemented using the SmallSQL database engine, which
implements a small subset of SQL. SmallSQL makes it much easier to keep files
and metadata together (particular in the case of groupdav where we have etags, 
and the funambol requirement of tracking names*). It also solves the threadsafe
problems that caused all hell with the old tracking code that was used in the
Funambol GroupDAV connector

Requirements:
-----------------------------------------------------
iCal4j 
SmallSQL+Commons Logging
